import React, { Component } from 'react'
import { QRCode } from 'react-qr-svg'
import htmlToImage from 'html-to-image'
import download from 'downloadjs'
import { Modal, Button, Dropdown, DropdownButton, InputGroup, FormControl } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css'

import './App.css'

var localValues = []
var localURL = []

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ig: '',
      url: [],
      setQR: false,
      showModal: false,
      names: ['ARC']
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleURL = this.handleURL.bind(this)
  }

  handleChange(event) {
    localValues = localValues.concat(event.target.value)
  }
  handleURL(event) {
    localURL = localURL.concat(event.target.value)
  }

  _setValue = (value) => {
    this.setState({ name: value })
  }
  _handleModal = () => {
    if (this.state.showModal === false)
      this.setState({ showModal: true })
    else
      this.setState({ showModal: false })
  }
  _saveNewValue = () => {
    this.setState({ showModal: false })
    this.setState({ names: this.state.names.concat(localValues[localValues.length-1]) })
  }
  _createQR = () => {
    this.setState({
      ig: this.state.name,
      url: localURL[localURL.length-1],
      setQR: true
    })
  }
  _downloadqr = () => {
    htmlToImage.toPng(document.getElementById('generated-qr'))
      .then(function (dataUrl) {
        download(dataUrl, 'my-node.png')
      })
  }

  render() {
    return(
      <div className='App'>
        <form>
          <div class='form-group'>
            <label for='exampleInputEmail1'>Name</label><br />
            <InputGroup>
              <FormControl
                placeholder='Name'
                value={ this.state.name }
              />

              <DropdownButton
                as={InputGroup.Append}
                variant='outline-secondary'
                title=''
                id='input-group-dropdown-2'
              >
                {
                  this.state.names.map((value, index) => {
                    return <Dropdown.Item onClick={ () => this._setValue(value) }>{value}</Dropdown.Item>
                  })
                }
                <Dropdown.Divider />
                <Dropdown.Item>
                  <a onClick={() => this._handleModal()}>Add new</a>

                  <Modal show={this.state.showModal} onHide={() => this._handleModal()}>
                    <Modal.Header closeButton>
                      <Modal.Title>Add new Project</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <input type='text' className='form-control' onChange={this.handleChange} />
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant='secondary' onClick={() => this._handleModal()}>Close</Button>
                      <Button variant='primary' onClick={() => this._saveNewValue()}>Save Changes</Button>
                    </Modal.Footer>
                  </Modal>

                </Dropdown.Item>
              </DropdownButton>
            </InputGroup>
          </div>
          <div class='form-group'>
            <label for='exampleInputPassword1'>URL</label>
            <input type='text' class='form-control' id='exampleInputPassword1' onChange={this.handleURL} placeholder='https://' />
          </div>
          <button type='button' class='btn btn-primary' onClick={() => this._createQR()}>Submit</button>
          <div class='form-group'></div>
          {
            this.state.setQR ?
              (
                <QRCode
                  id='generated-qr'
                  bgColor='#FFFFFF'
                  fgColor='#000000'
                  level='Q'
                  style={{ width: 256 }}
                  value={this.state.url+','+this.state.name}
                  onClick={() => this._downloadqr()}
                />
              )
            :
              (null)
          }
        </form>
      </div>
    )
  }
}
